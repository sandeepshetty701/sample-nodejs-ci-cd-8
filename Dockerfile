FROM node:10-alpine

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY node-express/package*.json /usr/src/app/
RUN npm install

COPY . /usr/src/app

RUN ls /usr/src/app/

# replace this with your application's default port
EXPOSE 3000
CMD [ "npm", "start" ]